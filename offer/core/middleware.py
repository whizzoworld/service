from rest_framework.authentication import TokenAuthentication

from offer.core.models import Token


class CustomTokenAuthentication(TokenAuthentication):
    model = Token
    keyword = "Token"