from django.conf.urls import url

from offer.core import views

urlpatterns = [
    url(r'^register/', views.UserRegistration.as_view(), name="register"),
    url(r'^login/', views.UserLogin.as_view(), name="login"),
    url(r'^change_password/', views.ChangePassword.as_view(), name="Change Password"),
]