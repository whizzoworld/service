# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from hashlib import sha1

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

# Create your models here.

GENDER_CHOICES = (
    ('M', 'male'),
    ('F', 'female'),
    ('O', 'other')
)


class Users(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    is_verified = models.BooleanField(default=False)
    mobile_number = models.BigIntegerField()
    gender = models.CharField(choices=GENDER_CHOICES, max_length=6)

    school_name = models.CharField(max_length=200, null=True)
    date_of_birth = models.DateField(null=True)
    address = models.TextField(null=True)
    city = models.CharField(max_length=50, null=True)
    pincode = models.IntegerField(null=True)
    country = models.CharField(max_length=50, null=True)
    display_picture = models.URLField(default='')

    class Meta:
        unique_together = ('email',)
        db_table = "users"


class TokenManager(models.Manager):
    def delete_session(self, *args, **kwargs):
        if "key" in kwargs:
            tokent_str = kwargs['key']
            tokens = Token.objects.filter(key=tokent_str).delete()
        return True

    def is_valid(self, *args, **kwargs):
        result = dict()
        if "key" in kwargs:
            tokent_str = kwargs['key']
            Token.objects.filter(
                key=tokent_str, expires_at__lte=timezone.now()).delete()
            tokens = Token.objects.filter(
                key=tokent_str, expires_at__gte=timezone.now()).get()
        return tokens


class Token(models.Model):
    user = models.ForeignKey(Users)
    key = models.CharField(primary_key=True, max_length=60)
    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField(default=None)
    objects = TokenManager()

    class Meta:
        db_table = 'access_tokens'
        db_tablespace = 'default'

    def save(self, *args, **kwargs):
        user_id_str = self.user.id
        self.expires_at = timezone.now() + settings.TOKEN_EXPIRE_TIME
        exists, self.key = self.generate_key(user_id_str)
        if not exists:
            super(Token, self).save(*args, **kwargs)
        return self

    def generate_key(self, id_):
        existing_token = None
        exists = True
        tokens = Token.objects.filter(
            user_id=id_, expires_at__gte=timezone.now()).values("key")
        for token in tokens:
            existing_token = token['key']
            token = existing_token
        if existing_token is None:
            exists = False
            token = sha1(id_.__str__() + str(timezone.now())).hexdigest()
            Token.objects.filter(
                user=id_, expires_at__lte=timezone.now()).delete()
        return exists, token
