from django.contrib.auth.hashers import check_password
from rest_framework import serializers

from offer.core import models


class SessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Users
        fields = ['email', 'first_name', 'last_name', 'display_picture']


class UserRegistrationSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='email')

    class Meta:
        model = models.Users
        fields = ('username', 'password', 'mobile_number',
                  'first_name', 'last_name', 'gender')


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=100)

    def authenticate_create_token(self):
        password = self.validated_data['password']
        user = models.Users.objects.filter(email=self.validated_data['email'])
        result = dict()
        if user.count() >= 1:
            user = user.get()
            pass_status = check_password(password, user.password)
            if pass_status:
                token = models.Token(user=user).save()
                result['status'] = True
                result['token'] = token.key
                return result
            else:
                result['status'] = False
                result['error'] = "Invalid Credentials"
                return result
        else:
            result['status'] = False
            result['error'] = "Invalid Credentials"
            return result

class ChangePasswordSerializer(serializers.Serializer):
	old_password = serializers.CharField(required=True)
	new_password = serializers.CharField(required=True)