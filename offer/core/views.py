# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.conf import settings
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from offer.core import serializers


# Create your views here.

class UserRegistration(GenericAPIView):
	""" User Registration """
	serializer_class = serializers.UserRegistrationSerializer
	parser_classes = ((JSONParser,FormParser))

	def post(self,request,*args,**kwargs):
		data = request.data
		s = serializers.UserRegistrationSerializer(data=data)
		result = dict()
		if s.is_valid():
			instance = s.save()
			instance.set_password(instance.password)
			instance.save()
			result['status'] = True
			return Response(result,status=status.HTTP_201_CREATED)
		else:
			result['status'] = False
			result['errors'] = s.errors
			return Response(result,status=status.HTTP_200_OK)

class UserLogin(GenericAPIView):
	""" User Login """
	serializer_class = serializers.UserLoginSerializer
	parser_classes = ((JSONParser,FormParser))
	
	def post(self,request,*args,**kwargs):
		s = serializers.UserLoginSerializer(data=request.data)
		result = dict()
		if s.is_valid():
			result = s.authenticate_create_token()
			return Response(result,status=status.HTTP_200_OK)
		else:
			result['status'] = False
			result['errors'] = s.errors
			return Response(result,status=status.HTTP_200_OK)

class ChangePassword(GenericAPIView):
	""" Change Password """
	permission_classes = (IsAuthenticated,)
	serializer_class = serializers.ChangePasswordSerializer

	def get_object(self, queryset=None):
		obj = self.request.user
		return obj

	def put(self, request, *args, **kwargs):
		temp = dict()
		self.object = self.get_object()
		serializer = self.get_serializer(data=request.data)
		if serializer.is_valid():
			if not self.object.check_password(serializer.data.get("old_password")):
				return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
			self.object.set_password(serializer.data.get("new_password"))
			self.object.save()
			temp['status'] = True
			temp['message'] = "success"
			return Response(temp, status=status.HTTP_200_OK)
		temp['status'] = False
		temp['message'] = serializer.errors
		return Response(temp, status=status.HTTP_400_BAD_REQUEST)